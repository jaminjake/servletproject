
package com.evans;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "Servlet", urlPatterns = {"/Servlet"})
public class Servlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        boolean tooShort = false;
        boolean tooLong = false;
        boolean validU = false;
        boolean validP = false;
        int length = 0;
        response.setContentType("text/html");
        out.println("<html><head></head><body>");
        String username = null;
        try {
            username = request.getParameter("username");
            length = username.length();
            if (length < 5) {
                out.println("<p>Username is too short.</p>");
                validU = false;
            } else if (length > 20) {
                out.println("<p>Username is too long.</p>");
                validU = false;
            /** } else if (length < 3 & length < 20) {
                valid = true; */
            } else {
                validU = true;
            }
        } catch (Exception e) {
            out.println("<p>Invalid Response. Please login again</p>");
        }
        String password = null;
        try {
            password = request.getParameter("password");
            length = password.length();
            if (length < 5) {
                out.println("<p>Password is too short.</p>");
                validP = false;
            } else if (length > 20) {
                out.println("<p>Password is too long.</p>");
                validP = false;
            /** } else if (length < 3 & length < 20) {
                valid = true; */
            } else {
                validP = true;
            }
        } catch (Exception e) {
            out.println("<p>Invalid Response. Please login again</p>");
        }
        if ((validU != true) && (validP = !true)) {
            out.println("<p>Please login again</p>");
        } else {
            double balance = (Math.random() * (password.length())) * (Math.random() * (password.length()));
            out.println("<h1>Welcome to Servelts Bank</h1>");
            out.println("<h3>Login Successful</h3>");
            out.println("<p>Welcome Back   " + username + "</p>");
            out.println("<p>Your Account Balance is :   " + String.format("%.2f", balance) + "</p>");
            out.println("</body></html>");
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("404 error - Page not found");

    }
}
